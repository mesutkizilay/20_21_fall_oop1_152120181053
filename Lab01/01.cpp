#include<iostream>
#include<fstream>
using namespace std;
int Sum(int*, int);
int Product(int*, int);
float Average(int*, int);
int Min(int*, int);
int main()
{
	fstream file;
	//!initializing variables
	int* A, x, counter = 0;
	//!opening for reading file
	file.open("input.txt", ios::in);

	if (!file)
	{
		cout << "The file wasn't opened!!! This error may be due to the file name being different." << endl;
		system("pause");
		return 0;
	}
	file >> x;
	if (x <= 0)
	{
		cout << "the number of values is not less than zero and is not equal to zero" << endl;
		system("pause");
		return 0;
	}
	A = new int[x];
	//!stored numbers into array
	while (!file.eof())
	{
		file >> A[counter];
		counter++;
	}
	cout << counter << endl;
	//!possible errors were checked.
	if (counter > x)
	{
		cout << "There are too much values in the file!!!" << endl;
		system("pause");
		return 0;
	}
	if (counter < x)
	{
		cout << "There are not enough values in the file!!!" << endl;
		system("pause");
		return 0;
	}
	//!printing the desired results to be calculated 
	cout << "Sum is:" << Sum(A, x) << endl;
	cout << "Product is:" << Product(A, x) << endl;
	cout << "Average is:" << Average(A, x) << endl;
	cout << "Min is:" << Min(A, x) << endl;
}
/**
* @brief Define sum function.
* @param sum is an integer variable that sums variables.
* @return return the sum of the variables.
*/
int Sum(int* A, int x)
{
	int sum = 0;
	for (int i = 0; i < x; i++)
	{
		sum = sum + A[i];
	}
	return sum;
}
/**
* @brief Define product function.
* @param pro is an integer variable that products variables.
* @return return the product of the variables.
*/
int Product(int* A, int x)
{
	int pro = 1;
	for (int i = 0; i < x; i++)
	{
		pro = pro * A[i];
	}
	return pro;
}
/**
* @brief Define avarage function.
* @return return the product of the variables.
*/
float Average(int* A, int x)
{
	return (float)Sum(A, x) / x;
}
/**
* @brief Define min function.
* @param min is an integer variable that find the smallest variables.
* @return return the smallest one of the variables.
*/
int Min(int* A, int x)
{
	int min = A[0];
	for (int i = 0; i < x; i++)
	{
		if (A[i] < min)
			min = A[i];
	}
	return min;
}