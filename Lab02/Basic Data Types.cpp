#include <iostream>
#include<iomanip>
using namespace std;

int main()
{
    int x; long y; char t; float z; double e;
    cin >> x >> y >> t >> z >> e;
    cout << x << endl << y << endl << t << endl;
    cout << fixed << setprecision(3) << z << endl;
    cout << fixed << setprecision(9) << e << endl;
    return 0;
}
