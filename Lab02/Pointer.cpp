#include <iostream>
using namespace std;
void update(int* a, int* b)
{
    int remainder = *a;
    *a = *a + *b;
    if (remainder > * b)
    {
        *b = remainder - *b;
    }
    else {
        *b = *b - remainder;
    }
}
int main()
{
    int a, b;
    int* pa = &a, * pb = &b;

    cin >> a >> b;
    update(pa, pb);
    cout << a << endl << b;
    return 0;
}
