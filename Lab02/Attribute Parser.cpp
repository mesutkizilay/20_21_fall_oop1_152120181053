#include <iostream>
#include <map>
#include<string>
using namespace std;

map <string, string> tagMap;

void createMap(int& n, string pretag) {
    if (!n) return;

    string row, tag, attr, value;
    getline(cin, row);

    int a = 1;
    if (row[a] == '/') {           // found closing tag
        while (row[a] != '>') a++;
        if (pretag.size() > (a - 2))        // update tag
            tag = pretag.substr(0, pretag.size() - a + 1);
        else
            tag = "";
    }
    else {                       // found opening tag
        while (row[a] != ' ' && row[a] != '>') a++;
        tag = row.substr(1, a - 1);    // update tag
        if (pretag != "") tag = pretag + "." + tag;

        int b;
        while (row[a] != '>') { // go through attributes
            b = ++a;
            while (row[a] != ' ') a++;
            attr = row.substr(b, a - b);    // attribute name

            while (row[a] != '\"') a++;
            b = ++a;
            while (row[a] != '\"') a++;
            value = row.substr(b, a - b);    // attribute value
            a += 1;

            tagMap[tag + "~" + attr] = value;
        }
    }
    createMap(--n, tag);
}
int main() {
    int n, x;
    cin >> n >> x;
    cin.ignore();
    createMap(n, "");

    string attr, de�er;
    while (x--) {
        getline(cin, attr);
        de�er = tagMap[attr];
        if (de�er == "") de�er = "Not Found!";
        cout << de�er << endl;
    }
    return 0;
}