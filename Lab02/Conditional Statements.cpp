#include <iostream>
using namespace std;

int main()
{
    int a;
    cin >> a;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    string say[10] = { "Greater than 9", "one", "two", "three", "four", "five", "six","seven", "eight", "nine" };

    if (a > 9) 
    {
        cout << say[0];
    }
    else 
    {
        cout << say[a];
    }

    return 0;
}
