#include <iostream>
#include <string>
using namespace std;

int main() 
{
    string str1;
    string str2;

    cin >> str1;
    cin >> str2;
    cout << str1.size() << " " << str2.size() << endl;
    cout << str1 + str2 << endl;
    cout << str2.front() + str1.substr(1, str1.size() - 1) << " " << str1.front() + str2.substr(1, str2.size() - 1);
    return 0;
}